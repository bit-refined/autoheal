# autoheal

A Docker container for monitoring and automatically restarting unhealthy Docker containers.

## Usage

### via `docker-compose`

```yaml
version: '3'
services:
  autoheal:
    image: registry.gitlab.com/bit-refined/autoheal
    restart: always
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```

## Configuration

By default `autoheal` will check every 60 seconds for unhealthy containers. Only containers with the label `autoheal=true` will be checked. These options can be changed using command line arguments.

**-a, --heal-all**: If given, check and restart all containers, ignoring labels.

**-l, --label <LABEL>**: Specify a different label to check. Containers need to have the given label set to `true` to be checked.

**-i, --interval <INTERVAL>**: Specify a different interval between checks.

**-v, --verbose**: Increase verbosity of output if specified.
