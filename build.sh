#!/usr/bin/env bash

# colors
bold=$(tput bold 2> /dev/null || echo -e "\x1b[1m")
red=$(tput setaf 1 2> /dev/null || echo -e "\x1b[38;5;1m")
green=$(tput setaf 2 2> /dev/null || echo -e "\x1b[38;5;2m")
yellow=$(tput setaf 3 2> /dev/null || echo -e "\x1b[38;5;3m")
blue=$(tput setaf 4 2> /dev/null || echo -e "\x1b[38;5;4m")
cyan=$(tput setaf 6 2> /dev/null || echo -e "\x1b[38;5;6m")
reset=$(tput sgr0 2> /dev/null || echo -e "\x1b[0m")

# misc. config
image_name="autoheal"
if [[ "${CI_COMMIT_BRANCH}" = "master" ]]; then
	image_tag="latest"
else
	image_tag="${CI_COMMIT_REF_SLUG}"
fi

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

set -ex

echo "${green}Building container${reset}"
container=$(buildah from --pull docker.io/debian:buster)
buildah run ${container} -- mkdir -p /app
buildah copy ${container} bin/* /app
buildah config --workingdir "/app" --entrypoint '["./autoheal"]' --cmd '' ${container}

echo "${green}Committing container${reset}"
buildah commit --format docker ${container} ${image_name}

if [[ -n "${CI_REGISTRY}" ]]; then
	echo -e "${green}Pushing image${reset}"
	buildah push ${image_name} "${CI_REGISTRY_IMAGE}:${image_tag}"
fi
