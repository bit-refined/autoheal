pub struct Config {
    heal_all: bool,
    label: String,
}

impl Config {
    pub fn new(heal_all: bool, label: String) -> Self {
        Self { heal_all, label }
    }

    pub fn heal_all(&self) -> bool {
        self.heal_all
    }

    pub fn label(&self) -> &str {
        &self.label
    }
}
