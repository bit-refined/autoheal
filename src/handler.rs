use std::collections::HashMap;

use bollard::{
    container::{APIContainers, ListContainersOptions, RestartContainerOptions},
    Docker,
};
use log::{error, info};

use crate::{util, Config};

pub async fn check_for_unhealthy_containers(client: &Docker, config: &Config) {
    let mut filters = HashMap::new();
    if !config.heal_all() {
        filters.insert("label".to_owned(), vec![format!("{}=true", config.label())]);
    }
    filters.insert("health".to_owned(), vec!["unhealthy".to_owned()]);
    let containers = match client
        .list_containers(Some(ListContainersOptions {
            filters,
            ..Default::default()
        }))
        .await
    {
        Ok(containers) => containers,
        Err(e) => {
            error!("Failed to get container information: {}", e);
            return;
        }
    };

    for container in containers {
        heal_container(client, container).await;
    }
}

async fn heal_container(client: &Docker, container: APIContainers) {
    info!(
        "Container {} ({}) is unhealthy, restarting",
        container.names.first().map_or_else(|| "", |s| s.as_str()),
        &container.id
    );
    if let Err(e) = client
        .restart_container::<RestartContainerOptions, &str>(&container.id, None)
        .await
    {
        error!("Failed to restart container: {}", e);
        return;
    }

    if let Some(services) = container.labels.get("autoheal.restart_services") {
        let services: Vec<_> = services.split(',').collect();
        let containers =
            match util::get_compose_service_containers(&client, &container, &services).await {
                Ok(containers) => containers,
                Err(e) => {
                    error!("Failed to get dependent containers to restart: {}", e);
                    return;
                }
            };
        for container in containers {
            info!(
                "Restarting dependent container {} ({})",
                container.names.first().map_or_else(|| "", |s| s.as_str()),
                &container.id
            );
            if let Err(e) = client
                .restart_container::<RestartContainerOptions, &str>(&container.id, None)
                .await
            {
                error!("Failed to restart container: {}", e);
            }
        }
    }
}
