use clap::value_t;
use log::{debug, info};
use tokio::{signal, stream, stream::StreamExt, time, time::Duration};

use config::Config;
use util::LogResult;

mod config;
mod handler;
mod util;

#[tokio::main]
async fn main() -> Result<(), ()> {
    let crate_setup = bin_common::CrateSetupBuilder::new()
        .with_app_name("autoheal")
        .build()
        .expect("CrateSetup");
    let matches = clap::App::new(crate_setup.application_name())
        .version(version::version!())
        .arg(
            clap::Arg::with_name("v")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Increase verbosity. May be specified multiple times."),
        )
        .arg(
            clap::Arg::with_name("heal-all")
                .short("a")
                .long("heal-all")
                .conflicts_with("label")
                .help("Heal all containers, even without label."),
        )
        .arg(
            clap::Arg::with_name("label")
                .short("l")
                .long("label")
                .value_name("LABEL")
                .conflicts_with("heal-all")
                .help("Label marking containers that should be healed.")
                .case_insensitive(true),
        )
        .arg(
            clap::Arg::with_name("interval")
                .short("i")
                .long("interval")
                .value_name("INTERVAL")
                .help("Interval in seconds between checks.")
                .default_value("60"),
        )
        .get_matches();
    let verbosity = if cfg!(debug_assertions) { 2 } else { 1 } + matches.occurrences_of("v") as u8;
    crate_setup
        .logging_setup()
        .with_verbosity(verbosity)
        .with_log_panics(true)
        .with_log_to_file(false)
        .build()
        .expect("Failed to setup output");
    let interval = value_t!(matches, "interval", u64).unwrap_or_else(|e| e.exit());

    info!(
        "Starting {} v{}",
        crate_setup.application_name(),
        version::version!()
    );
    let config = Config::new(
        matches.is_present("heal-all"),
        matches.value_of("label").unwrap_or("autoheal").to_owned(),
    );

    let docker = bollard::Docker::connect_with_local_defaults()
        .unwrap_and_log("Failed to connect to docker daemon")?;

    let version = docker
        .version()
        .await
        .unwrap_and_log("Failed to get version from docker daemon")?;
    log::info!("Docker version: {}", version.version);

    let mut interval = time::interval(Duration::from_secs(interval));
    let mut signal_stream: Box<dyn stream::Stream<Item = ()> + Unpin> =
        Box::new(stream::pending::<()>());
    if let Ok(signal) = signal::unix::signal(signal::unix::SignalKind::interrupt()) {
        signal_stream = Box::new(StreamExt::merge(signal_stream, signal));
    }
    if let Ok(signal) = signal::unix::signal(signal::unix::SignalKind::terminate()) {
        signal_stream = Box::new(StreamExt::merge(signal_stream, signal));
    }
    loop {
        tokio::select! {
            _ = interval.tick() => {
                debug!("Checking for unhealthy containers");
                handler::check_for_unhealthy_containers(&docker, &config).await;
            }
            _ = signal_stream.next() => {
                info!("Signal received, exiting");
                break;
            }
        }
    }

    Ok(())
}
