use std::collections::HashMap;

use bollard::{
    container::{APIContainers, ListContainersOptions},
    errors::Error,
    Docker,
};
use log::error;

pub trait LogResult<T> {
    fn unwrap_and_log(self, msg: &'static str) -> Result<T, ()>;
}

impl<T, E: std::fmt::Display> LogResult<T> for Result<T, E> {
    fn unwrap_and_log(self, msg: &'static str) -> Result<T, ()> {
        match self {
            Ok(t) => Ok(t),
            Err(e) => {
                error!("{}: {}", msg, e);
                Err(())
            }
        }
    }
}

pub async fn get_compose_service_containers(
    client: &Docker,
    container: &APIContainers,
    services: &[&str],
) -> Result<Vec<APIContainers>, Error> {
    let (project, config_files, working_dir) = match (
        container.labels.get("com.docker.compose.project"),
        container
            .labels
            .get("com.docker.compose.project.config_files"),
        container
            .labels
            .get("com.docker.compose.project.working_dir"),
    ) {
        (Some(project), Some(config_files), Some(working_dir)) => {
            (project, config_files, working_dir)
        }
        _ => return Ok(vec![]),
    };

    let mut filters = HashMap::new();
    filters.insert(
        "label".to_owned(),
        vec![
            format!("com.docker.compose.project={}", project),
            format!("com.docker.compose.project.config_files={}", config_files),
            format!("com.docker.compose.project.working_dir={}", working_dir),
        ],
    );
    let containers = client
        .list_containers(Some(ListContainersOptions {
            filters,
            ..Default::default()
        }))
        .await?
        .drain(..)
        .filter(|c| {
            if let Some(service) = c.labels.get("com.docker.compose.service") {
                services.contains(&service.as_str())
            } else {
                false
            }
        })
        .collect();

    Ok(containers)
}
